

class Urls{
// static final baseUrl = "https://telecaller.dev.wecodelife.com/";
  static final baseUrl = "https://telecaller.app.vazhemadomprints.com/";
static final loginUrl =baseUrl+ "api/v1/login/";
// static final getCustomerUrl = baseUrl+"api/get-customer/";
  static final custDetailsUrl = baseUrl+"api/customer/get-customer-by-user/";
static final feildReportUrl = baseUrl+"api/customer/field-report/";
static final logoutUrl = baseUrl+"api/logout/";
static final updateStatusUrl = baseUrl+"api/status/update/";
static final uploadFileUrl =baseUrl+ "api/files/";
static final answerQuestionUrl = baseUrl+"api/add-answer/";
static final phoneNumUrl = baseUrl+"api/agent/phone-status/";
static final addRemark = baseUrl+"api/agent/add-agent-remarks/";

}