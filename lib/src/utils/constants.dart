import 'dart:ui';

class Constants {

  ///error
  static const String SOME_ERROR_OCCURRED = "Some error occurred.";


  ///validators
  static const String EMAIL_NOT_VALID = "Email is not valid";
  static const String USERNAME_NOT_VALID = "Username is not valid";
  static const String PASSWORD_LENGTH =
      "Password length should be greater than 5 chars.";
  static const String INVALID_MOBILE_NUM = "Invalid mobile number";
  static const String INVALID_NAME = "Invalid name";
  static const String BOX_NAME ="app";
  static const String baseUrl = "Feature not available";

  static List<Color> kitGradients = [
    Color(0xFFE5E5E5),
    Color(0xFFFFFFFF),
    Color(0xFF6C6C6C),
    Color(0xFF016BA6),
    Color(0xFF5E5E5E),
    Color(0xFFF0EFEF), //5
    Color(0xFFC4C4C4),
    Color(0xFFEEEEEE),
  ];
}
