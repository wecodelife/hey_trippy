import 'package:hey_trippy/src/api_providers/repository/repository.dart';
import 'package:hey_trippy/src/utils/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'api_client.dart';


/// it is a hub that connecting pref,repo,client
/// used to reduce imports in pages
class ObjectFactory {
  static final _objectFactory = ObjectFactory._internal();

  ObjectFactory._internal();

  factory ObjectFactory() => _objectFactory;

  ///Initialisation of Objects
  // Prefs _prefs = Prefs();
  ApiClient _apiClient = ApiClient();
  Repository _repository = Repository();
  AppHive _appHive =AppHive();
  // GetMonthAlpha _getMonthAlpha=GetMonthAlpha();
  // GetDay _getDay=GetDay();

  ///
  /// Getters of Objects
  ///
  ApiClient get apiClient => _apiClient;
  //
  // Prefs get prefs => _prefs;
  AppHive get appHive => _appHive;
  Repository get repository => _repository;

  // GetMonthAlpha get getMonthAlpha => _getMonthAlpha;
  //
  // GetDay get getDay => _getDay;

  ///
  /// Setters of Objects
  ///
  ///
  final pref = SharedPreferences.getInstance();

  void setPrefs(SharedPreferences sharedPreferences) async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    // _prefs.sharedPreferences = sharedPreferences.;
  }
}
