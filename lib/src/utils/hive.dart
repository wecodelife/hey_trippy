
import 'package:flutter/cupertino.dart';
import 'package:hey_trippy/src/utils/constants.dart';
import 'package:hive/hive.dart';


class AppHive{

  static const String _TOKEN= "token";
  static const String _CUSTOMERNAME = 'custname';

  void hivePut({String key,String value})async{

    await Hive.box(Constants.BOX_NAME).put(key,value);

  }


 String hiveGet({String key}){

    return Hive.box(Constants.BOX_NAME).get(key);
 }

  
  putToken({String token}){
    
    return hivePut(key:_TOKEN,value:token);
 }

  getToken(){

    return hiveGet(key:_TOKEN);
  }

  void deleteToken(){
    putToken(token: "");

  }

  customerNamePut({String token}){

    return hivePut(key:_CUSTOMERNAME,value:token);
  }

  getCustomerName(){

    return hiveGet(key:_CUSTOMERNAME);
  }



  AppHive();
}