import 'package:hey_trippy/src/models/notification_list_response.dart';
import 'package:hey_trippy/src/models/state.dart';
import 'package:hey_trippy/src/utils/object_factory.dart';

import 'package:quiver/collection.dart';




class UserApiProvider {
  Future<State> sampleCall() async {
    final response = await ObjectFactory().apiClient.sampleApiCall();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<SampleResponseModel>.success(
          SampleResponseModel.fromJson(response.data));
    } else {
      return null;
    }
  }





}


