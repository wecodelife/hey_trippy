import 'package:hey_trippy/src/api_providers/user_api_provider.dart';
import 'package:hey_trippy/src/models/state.dart';


/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> sampleCall() => userApiProvider.sampleCall();






}
