import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:hey_trippy/src/utils/utils.dart';

class TravelPlan extends StatefulWidget {
  @override
  _TravelPlanState createState() => _TravelPlanState();
}

class _TravelPlanState extends State<TravelPlan> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 11) * 5,
      width: screenWidth(context, dividedBy: 1),
      child: ListView.builder(
        itemCount: 5,
        itemBuilder: (BuildContext, int index) {
          return Container(
            height: screenHeight(context, dividedBy: 11),
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 80),
                  width: screenWidth(context, dividedBy: 10),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.cyan,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 55)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Arrival in Goa via flight",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.black,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600)),
                      Text("Air Asia -2h 45m - nonstop",
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.w600))
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
