import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hey_trippy/src/utils/utils.dart';

class GridSmall extends StatefulWidget {
  @override
  _GridSmallState createState() => _GridSmallState();
}

List<String> image = ['assets/images/place2.jpg'];

class _GridSmallState extends State<GridSmall> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: screenHeight(context, dividedBy: 700),
          ),
          child: Container(
            height: screenHeight(context, dividedBy: 5.2),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Image(
                image: AssetImage(image[0]),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: screenHeight(context, dividedBy: 200),
            left: screenWidth(
              context,
              dividedBy: 17,
            ),
          ),
          child: Text("Munnar",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w600)),
        ),
        Padding(
          padding: EdgeInsets.only(
              left: screenWidth(
                context,
                dividedBy: 17,
              ),
              top: screenHeight(context, dividedBy: 200)),
          child: Text("Munnar",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w600)),
        ),
      ],
    ));
  }
}
