import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hey_trippy/src/utils/utils.dart';

import 'GridSmall.dart';

class GridBig extends StatefulWidget {
  @override
  _GridBigState createState() => _GridBigState();
}

List<String> image = [
  'assets/images/place1.jpg',
  'assets/images/place2.jpg',
  'assets/images/place3.jpg'
];

class _GridBigState extends State<GridBig> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        body: StaggeredGridView.countBuilder(
          crossAxisCount: 4,
          itemCount: 8,
          itemBuilder: (BuildContext context, int index) => Padding(
            padding: EdgeInsets.all(4),
            child: new Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(-0.4, -0.4),
                          color: Color(0xFFDBE7EB),
                          blurRadius: 1),
                      BoxShadow(
                          offset: Offset(0.4, 0.4),
                          color: Color(0xFFDBE7EB),
                          blurRadius: 1)
                    ]),
                child: Column(
                  children: [
                    index.isEven
                        ? Container(
                            child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                  top: screenHeight(context, dividedBy: 700),
                                ),
                                child: Container(
                                  height: screenHeight(context, dividedBy: 3.2),
                                  width: screenWidth(context, dividedBy: 2.1),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20),
                                    child: Image(
                                      image: AssetImage(image[0]),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: screenHeight(
                                      context,
                                      dividedBy: 200,
                                    ),
                                    left: screenWidth(
                                      context,
                                      dividedBy: 20,
                                    )),
                                child: Text("Alappuzha",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600)),
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                    top: screenHeight(
                                      context,
                                      dividedBy: 500,
                                    ),
                                    left: screenWidth(
                                      context,
                                      dividedBy: 20,
                                    )),
                                child: Text("Alappuzha",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontFamily: 'Poppins',
                                        fontWeight: FontWeight.w600)),
                              ),
                            ],
                          ))
                        : GridSmall(),
                  ],
                )),
          ),
          staggeredTileBuilder: (int index) =>
              new StaggeredTile.count(2, index.isEven ? 3 : 2),
          mainAxisSpacing: screenHeight(context, dividedBy: 40),
          crossAxisSpacing: screenWidth(context, dividedBy: 100),
        ));
  }
}
