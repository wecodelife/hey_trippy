import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hey_trippy/src/utils/utils.dart';

class ListViewImages extends StatefulWidget {
  @override
  _ListViewImagesState createState() => _ListViewImagesState();
}

List<String> image = [
  "assets/images/place1.jpg",
  "assets/images/place2.jpg",
  "assets/images/place3.jpg",
];

class _ListViewImagesState extends State<ListViewImages> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 1.6),
      width: screenWidth(context, dividedBy: 1),
      child: ListView.builder(
          itemCount: image.length,
          padding: EdgeInsets.only(),
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext, int index) {
            return Padding(
              padding: EdgeInsets.only(
                left: screenWidth(context, dividedBy: 40),
                right: screenWidth(
                  context,
                  dividedBy: 300,
                ),
                top: screenHeight(context, dividedBy: 500),
              ),
              child: Container(
                  height: screenHeight(context, dividedBy: 1.6),
                  width: screenWidth(context, dividedBy: 1.05),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(30),
                    child: Image(
                      image: AssetImage(image[index]),
                      fit: BoxFit.cover,
                    ),
                  )),
            );
          }),
    );
  }
}
