import 'package:flutter/material.dart';
import 'package:hey_trippy/src/utils/constants.dart';
import 'package:hey_trippy/src/utils/utils.dart';


import 'list_view__travel_plans.dart';

class ListViewDailyPlans extends StatefulWidget {
  @override
  _ListViewDailyPlansState createState() => _ListViewDailyPlansState();
}

class _ListViewDailyPlansState extends State<ListViewDailyPlans> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 1.7),
      width: screenHeight(context, dividedBy: 1.2),
      child: ListView.builder(
          itemCount: 2,
          itemBuilder: (BuildContext, int Index) {
            return Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 200),
                      left: screenWidth(context, dividedBy: 32)),
                  child: Text(
                    "Daily Plans",
                    style: TextStyle(
                        fontSize: 20,
                        color: Constants.kitGradients[4],
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w600),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 50)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                          left: screenWidth(context, dividedBy: 30),
                        ),
                        child: Container(
                            height: screenHeight(context, dividedBy: 15),
                            width: screenWidth(context, dividedBy: 150),
                            color: Constants.kitGradients[4]),
                      ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: screenWidth(context, dividedBy: 30)),
                              child: Row(
                                children: [
                                  Text(
                                    "Day 1. 21 Nov",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Constants.kitGradients[4],
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  left: screenWidth(
                                    context,
                                    dividedBy: 30,
                                  ),
                                  top: screenHeight(context, dividedBy: 150)),
                              child: Text(
                                "Flight to Goa.Check-in to Goa.",
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ]),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 40)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Details",
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.blue,
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(width: screenWidth(context, dividedBy: 1.5)),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 15,
                        color: Colors.blue,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Divider(
                  color: Colors.grey,
                  thickness: 1.0,
                ),
                TravelPlan(),
              ],
            ));
          }),
    );
  }
}
