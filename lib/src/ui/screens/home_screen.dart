import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:hey_trippy/firebase_auth/auth_service.dart';
import 'package:hey_trippy/src/ui/screens/place_description.dart';
import 'package:hey_trippy/src/ui/widgets/grid_big.dart';
import 'package:hey_trippy/src/utils/utils.dart';

import 'login_page.dart';

class Home_screen extends StatefulWidget {
  FirebaseAuth auth;
  Home_screen({this.auth});
  @override
  _Home_screenState createState() => _Home_screenState();
}

class _Home_screenState extends State<Home_screen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: screenHeight(
                      context,
                      dividedBy: 15,
                    ),
                    left: screenWidth(context, dividedBy: 25),
                  ),
                  child: Container(
                    height: screenHeight(
                      context,
                      dividedBy: 17,
                    ),
                    width: screenWidth(context, dividedBy: 11),
                    child: Icon(Icons.sort, color: Colors.black, size: 40),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: screenHeight(
                      context,
                      dividedBy: 20,
                    ),
                    left: screenWidth(context, dividedBy: 1.5),
                  ),
                  child: GestureDetector(
                    child: Container(
                      height: screenHeight(
                        context,
                        dividedBy: 30,
                      ),
                      width: screenWidth(context, dividedBy: 11),
                      child: Icon(Icons.person_outline,
                          color: Colors.black, size: 35),
                    ),
                    onTap: () {
                      signOut(widget.auth);
                    },
                  ),
                ),
              ],
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 25)),
              child: Container(
                height: screenHeight(context, dividedBy: 15),
                width: screenWidth(context, dividedBy: 1.2),
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                    offset: Offset(-0.4, -0.4),
                    color: Color(0xFFDBE7EB),
                    blurRadius: 3,
                    spreadRadius: 3,
                  ),
                  BoxShadow(
                    offset: Offset(0.4, 0.4),
                    color: Color(0xFFDBE7EB),
                    blurRadius: 3,
                    spreadRadius: 3,
                  )
                ]),
                child: TextField(
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(
                      top: screenHeight(
                        context,
                        dividedBy: 50,
                      ),
                      left: screenWidth(context, dividedBy: 15),
                    ),
                    hintText: "Search here",
                    hintStyle: TextStyle(
                        color: Colors.grey,
                        fontFamily: 'Poppins',
                        fontSize: 16),
                    suffixIcon: Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 30)),
                      child: Icon(Icons.search_outlined, color: Colors.black54),
                    ),
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            Padding(
              padding:
                  EdgeInsets.only(top: screenHeight(context, dividedBy: 40)),
              child: Container(
                  color: Colors.white,
                  height: screenHeight(context, dividedBy: 1.35),
                  width: screenWidth(context, dividedBy: 1.03),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PlaceDescription()),
                            (route) => false);
                      },
                      child: GridBig())),
            ),
          ],
        ),
      ),
    );
  }

  signOut(FirebaseAuth auth) async {
    String status = await Auth(auth: auth).Signout();
    if (status.trim() == "Success") {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => HomePage(
                    auth: auth,
                  )));
      print("Successfully signed out");
    }
  }
}
