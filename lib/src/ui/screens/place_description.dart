import 'package:flutter/material.dart';
import 'package:hey_trippy/src/ui/screens/home_screen.dart';
import 'package:hey_trippy/src/ui/widgets/list_view_daily_plans.dart';
import 'package:hey_trippy/src/ui/widgets/list_view_images.dart';
import 'package:hey_trippy/src/utils/constants.dart';
import 'package:hey_trippy/src/utils/utils.dart';

class PlaceDescription extends StatefulWidget {
  @override
  _PlaceDescriptionState createState() => _PlaceDescriptionState();
}

class _PlaceDescriptionState extends State<PlaceDescription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => Home_screen()),
                  (route) => false);
            },
            child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 20),
                      left: screenWidth(context, dividedBy: 20)),
                  child: Icon(
                    Icons.arrow_back,
                    color: Constants.kitGradients[4],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 20),
                      left: screenWidth(context, dividedBy: 20)),
                  child: Text(
                    "Back",
                    style: TextStyle(
                        fontSize: 18,
                        color: Constants.kitGradients[4],
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.w600),
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 50)),
            child: Container(
              height: screenHeight(context, dividedBy: 1.13),
              child: Padding(
                padding: EdgeInsets.only(),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      ListViewImages(),
                      Padding(
                        padding: EdgeInsets.only(
                            left: screenHeight(
                              context,
                              dividedBy: 50,
                            ),
                            top: screenWidth(context, dividedBy: 20)),
                        child: Row(
                          children: [
                            Text("The secret trails of munnar",
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Constants.kitGradients[4],
                                    fontFamily: 'Poppins',
                                    fontWeight: FontWeight.bold)),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: screenWidth(context, dividedBy: 20),
                            right: screenWidth(context, dividedBy: 20)),
                        child: Container(
                          width: screenWidth(context, dividedBy: 1),
                          child: Text(
                            "Munnar is a town and hill station located in the Idukki district of the southwestern Indian state of Kerala. Munnar is situated at around 1,600 metres above mean sea level, in the Western Ghats mountain range. Munnar is also called the 'Kashmir of South India' and is a popular honeymoon destination.",
                            style: TextStyle(
                                fontSize: 12,
                                color: Constants.kitGradients[4],
                                fontFamily: 'Poppins',
                                fontWeight: FontWeight.bold),
                            overflow: TextOverflow.clip,
                          ),
                        ),
                      ),
                      ListViewDailyPlans()
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
