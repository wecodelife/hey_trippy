import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:hey_trippy/firebase_auth/auth_service.dart';
import 'package:hey_trippy/src/ui/screens/home_screen.dart';

class HomePage extends StatelessWidget {
  FirebaseAuth auth;
  HomePage({this.auth});
  @override
  Widget build(BuildContext context) {
    return NeumorphicTheme(
      theme: NeumorphicThemeData(
          defaultTextColor: Color(0xFF3E3E3E),
          accentColor: Colors.grey,
          variantColor: Colors.black38,
          depth: 8,
          intensity: 0.65),
      usedTheme: UsedTheme.LIGHT,
      child: Material(
        child: NeumorphicBackground(
          child: _Page(
            auth: auth,
          ),
        ),
      ),
    );
  }
}

class _Page extends StatefulWidget {
  FirebaseAuth auth;
  _Page({this.auth});
  @override
  State<StatefulWidget> createState() => _PageState();
}

enum Gender { MALE, FEMALE, NON_BINARY }

class _PageState extends State<_Page> {
  String firstName = "";
  String lastName = "";
  Gender gender;
  Set<String> rides = Set();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SingleChildScrollView(
            child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // Container(
        //   margin: EdgeInsets.only(left: 12, right: 12, top: 10),
        //   child: TopBar(
        //     actions: <Widget>[ThemeConfigurator()],
        //   ),
        // ),
        Neumorphic(
          margin: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
          boxShape: NeumorphicBoxShape.roundRect(BorderRadius.circular(12)),
          style: NeumorphicStyle(),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 50,
              ),
              _AvatarField(),
              SizedBox(
                height: 15,
              ),
              _TextField(
                label: "Mobile number",
                hint: "",
                onChanged: (firstName) {
                  setState(() {
                    this.firstName = firstName;
                  });
                },
              ),
              SizedBox(
                height: 20,
              ),
              Signin(
                auth: widget.auth,
              ),
              SizedBox(
                height: 8,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        )
      ],
    )));
  }

  bool _isButtonEnabled() {
    return this.firstName.isNotEmpty && this.lastName.isNotEmpty;
  }
}

class _AvatarField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Neumorphic(
        boxShape: NeumorphicBoxShape.circle(),
        padding: EdgeInsets.all(30),
        style: NeumorphicStyle(
          depth: NeumorphicTheme.embossDepth(context),
        ),
        child: Icon(
          Icons.directions_bike_sharp,
          size: 100,
          color: Colors.black.withOpacity(0.2),
        ),
      ),
    );
  }
}

class _TextField extends StatefulWidget {
  final String label;
  final String hint;

  final ValueChanged<String> onChanged;

  _TextField({@required this.label, @required this.hint, this.onChanged});

  @override
  __TextFieldState createState() => __TextFieldState();
}

class __TextFieldState extends State<_TextField> {
  TextEditingController _controller;

  @override
  void initState() {
    _controller = TextEditingController(text: widget.hint);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8),
          child: Text(
            this.widget.label,
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: NeumorphicTheme.defaultTextColor(context),
            ),
          ),
        ),
        Neumorphic(
          margin: EdgeInsets.only(left: 8, right: 8, top: 2, bottom: 4),
          boxShape: NeumorphicBoxShape.stadium(),
          style: NeumorphicStyle(depth: NeumorphicTheme.embossDepth(context)),
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 18),
          child: TextField(
            onChanged: this.widget.onChanged,
            controller: _controller,
            decoration: InputDecoration.collapsed(hintText: this.widget.hint),
          ),
        )
      ],
    );
  }
}

class Signin extends StatefulWidget {
  FirebaseAuth auth;
  Signin({this.auth});
  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8),
          child: Text(
            "Sign in ",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: NeumorphicTheme.defaultTextColor(context),
            ),
          ),
        ),
        Row(
          children: <Widget>[
            SizedBox(width: 12),
            NeumorphicRadio(
              padding: EdgeInsets.all(20),
              boxShape: NeumorphicBoxShape.circle(),
              child: Image.asset(
                'assets/images/google_icon.png',
                width: 25,
                height: 25,
              ),
              onChanged: (value) {
                signInGoogle(widget.auth);
              },
            ),
            SizedBox(width: 12),
            NeumorphicRadio(
              padding: EdgeInsets.all(20),
              boxShape: NeumorphicBoxShape.circle(),
              child: Image.asset(
                'assets/images/facebook_icon.png',
                width: 25,
                height: 25,
              ),
              onChanged: (value) {},
            ),
            SizedBox(width: 12),
            NeumorphicRadio(
              padding: EdgeInsets.all(25),
              boxShape: NeumorphicBoxShape.circle(),
              child: Center(
                child: Text('Get OTP'),
              ),
              onChanged: (value) {},
            ),
            SizedBox(
              width: 18,
            )
          ],
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }

  signInGoogle(FirebaseAuth auth) async {
    String status = await Auth(auth: auth).signInWithGoogle();
    if (status.trim() == "Success") {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => Home_screen(
                    auth: auth,
                  )),
          (route) => false);
      print("Successfully signed in with google");
    }
  }
}
